import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

describe('App Component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('App Component Snapshots', () => {
  it('Snapshot', () => {
      const app = shallow(<App />);
      expect(toJson(app)).toMatchSnapshot();
  });
});

describe('App Component', () => {
  it('renders without crashing & validate Text', () => {
    const wrapper = shallow(<App />);
    const text = wrapper.find('p').text();
    expect(text).toEqual('To-Do List App (React + Jest + Enzime)');
  });
});