import "./App.css";
import React, { Component } from 'react';
import TodoApp from "./components/TodoApp/TodoApp";

class App extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
    }
  }

  makeIncrementer = amount => () =>
    this.setState(prevState => ({
      count: prevState.count + amount,
    }));

  increment = this.makeIncrementer(1);

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            To-Do List App (React + Jest + Enzime)
          </p>
          <TodoApp />
        </header>
      </div>
    )
  }
}

export default App;