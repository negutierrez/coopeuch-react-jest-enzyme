import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from "enzyme";
import TodoList from './TodoList';
import { v4 as uuid } from "uuid";
import toJson from "enzyme-to-json";

const parameters = {
    list_items: [
        { text: 'This is the first To-Do Item', id: uuid() },
        { text: 'This is the second To-Do Item', id: uuid() }
    ]
}

describe('To-Do List Component', () => {
    it('renders TodoList without crashing', () => {
        shallow(<TodoList todos={parameters.list_items} remove={() => {console.log('Test')}}/>);
    });
});

describe('To-Do List Snapshots', () => {
    it('Snapshot', () => {
        const tree = shallow(<TodoList todos={parameters.list_items} remove={() => {console.log('Test')}}/>);
        expect(toJson(tree)).toMatchSnapshot();
    });
});