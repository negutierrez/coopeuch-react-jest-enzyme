import { TodoItem } from "../TodoItem/TodoItem";
import { Component } from "react";

class TodoList extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const todoNode = this.props.todos.map((todo) => {
            return (<TodoItem todo={todo} key={todo.id} remove={this.props.remove} />)
        });

        return (
            <ul className="list">
                {todoNode}
            </ul>
        )
    }
}

export default TodoList;