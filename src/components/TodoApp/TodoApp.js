import { Component } from "react";
import { v4 as uuid } from "uuid";
import TodoList from "../TodoList/TodoList";
import { TodoForm } from "../TodoForm/TodoForm";
import { TodoTitle } from "../TodoTitle/TodoTitle";


class TodoApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list_items: []
        }
    }

    addTodoItem(val) {
        const todo = { text: val, id: uuid() }

        this.state.list_items.push(todo);

        this.setState({list_items: this.state.list_items});
    }

    removeTodoItem(id) {
        const listItemsFiltered = this.state.list_items.filter((todo) => {
            if (todo.id !== id) return todo;
        });

        this.setState({list_items: listItemsFiltered});
    }

    render() {
        return (
            <div>
                <TodoTitle todoCount={this.state.list_items.length}/>
                <TodoForm addTodo={this.addTodoItem.bind(this)} />
                <TodoList
                    todos={this.state.list_items}
                    remove={this.removeTodoItem.bind(this)}
                />
            </div>
        )
    }
}

export default TodoApp