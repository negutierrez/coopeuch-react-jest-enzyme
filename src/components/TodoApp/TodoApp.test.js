import React from "react";
import TodoApp from './TodoApp';
import toJson from "enzyme-to-json";
import { shallow, mount } from "enzyme";

describe('To-Do App Component', () => {
    it('renders TodoApp without crashing', () => {
        shallow(<TodoApp />);
    });
});

describe('To-Do App Snapshots', () => {
    it('Snapshot', () => {
        const tree = shallow(<TodoApp />);
        expect(toJson(tree)).toMatchSnapshot();
    });
});

describe('To-Do App Logic', () => {
    const wrapper = mount(<TodoApp />);

    it('Validating logic Add Item', () => {
        const input = wrapper.find('input').first();
        input.instance().value = 'Test';
        wrapper.find('form').simulate('submit');
        const numerTitle = wrapper.find('.text-balance').text();
        const titleApp = wrapper.find('h1').text();
        expect(titleApp).toEqual(`You have (${numerTitle}) items to do`);
    });

    it('Validating logic Remove Item', () => {
        const firstItem = wrapper.find('.list-group-item').first();
        firstItem.simulate('click');
        expect(wrapper.instance().state.list_items.length).toEqual(0);
    });
});