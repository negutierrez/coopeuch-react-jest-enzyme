export const TodoTitle = ({todoCount}) => {
    return (
        <div>
            <div>
                <h1>
                    You have (<span className="text-balance">{todoCount}</span>) items to do
                </h1>
            </div>
        </div>
    )
}