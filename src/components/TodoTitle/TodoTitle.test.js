import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { TodoTitle } from "./TodoTitle";

describe('To-Do Title Component', () => {
    it('TodoTitle renders without crashing', () => {
        const titleComponent = document.createElement('div');
        ReactDOM.render(<TodoTitle todoCount="0"/>, titleComponent);
        ReactDOM.unmountComponentAtNode(titleComponent);
    });
});

describe('To-Do Title Component', () => {
    it('starts with count of 0', () => {
        const wrapper = shallow(<TodoTitle todoCount="0"/>);
        const text = wrapper.find('h1').text();
        expect(text).toEqual('You have (0) items to do')
    })
});

describe('To-Do Title Snapshots', () => {
    it('Snapshot', () => {
        const tree = shallow(<TodoTitle todoCount="0"/>);
        expect(toJson(tree)).toMatchSnapshot();
    });
});