export const TodoItem = ({todo, remove}) => {
    return (
        <li className="list-group-item" onClick={() => { remove(todo.id)}}>
            {todo.text}
        </li>
        // <a href="#" className="list-group-item" onClick={() => {remove(todo.id)}}>{todo.text}</a>
    );
}